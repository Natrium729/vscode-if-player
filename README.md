# IF Player extension for VS Code

This extension adds custom editors to open and play some interactive fiction formats, directly in VS Code.

## Features

With this extension, you can open these files in VS Code to play them:

- Z-machine files (`.z3`, `.z4`, `.z5`, `.z8` and `.zblorb` file extensions)
- Glulx files (`.ulx` and `.gblorb` file extensions)
- Compiled ink files (`.ink.json` file extension); note that plain JSON files (without `.ink` before `.json`) cannot be opened as ink stories.

A story file cannot be opened in more than one tab, but the game state will be kept even if the tab is in the background.

If the file is in the currently opened workspace, its corresponding tab will be reloaded every time the file is updated. This can be useful when authoring a game: compile the story and the story tab will automatically load the new version.

Alternatively, you can click on the "Reload" button during play to reload the story.

The story tabs respect your current VS Code theme colours. You can further customise the appearance in the settings.

## Extension settings

All the settings are prefixed with `ifPlayer.` (e.g. `ifPlayer.appearance.lineHeight`)

- `appearance.fontSize`: The font size in story tabs. 0 means the font size of the VS Code editor will be used.
- `appearance.fontFamily`: The main font family in story tabs. If left empty, Georgia will be used.
- `appearance.fontFamilyMonospace`: The font family for monospace texts in story tabs. Used for example in the status bars.
- `appearance.lineHeight`: The line height of the text in story tabs.

## Licence

This extension is released under the MIT License; see "LICENSE" file.

It includes the [Quixe interpreter](https://github.com/erkyrath/quixe) by Andrew Plotkin; the [ZVM interpreter](https://github.com/curiousdannii/ifvms.js) by Dannii Willis and other contributors; [inkjs](https://github.com/y-lohse/inkjs) by Yannick Lohse; and [jQuery](https://github.com/jquery/jquery). They are all released under the MIT License.

It also includes the [Å-machine engine](https://linusakesson.net/dialog/aamachine/index.php) by Linus Åkesson, which is released under the 2-clause BSD license (see the header of `webview-resources/aa-machine/aa-engine.js` in this repository).
