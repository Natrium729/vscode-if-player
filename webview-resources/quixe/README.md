# Quixe

This folder contains Quixe 2.1.7 by Andrew Plotkin, based on the Inform 7 template downloadable on the [Quixe homepage](https://eblong.com/zarf/glulx/quixe/).

## License

The Quixe, GiDispa, and GiLoad Javascript libraries are copyright 2010-20 by Andrew Plotkin. They are distributed under the MIT license; see the "LICENSE" file.
