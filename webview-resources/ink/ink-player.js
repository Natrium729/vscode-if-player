const maxHistoryLength = 50

window.inkPlayer = {
	story: null,
	messages: [],
	storyContainer: null,
	currentTurnElement: null,
	history: [],
	currentTurnState: "",

	start(data) {
		this.storyContainer = document.getElementById("story-container")
		try {
			this.story = new inkjs.Story(data)
		} catch (error) {
			let errorElement = document.createElement("p")
			errorElement.classList.add("error")
			errorElement.innerText = "This file doesn't seem to contain an ink story."
			this.storyContainer.appendChild(errorElement)
			return
		}

		this.story.onError = (msg, type) => {
			switch (type) {
				case 0: // Author.
					this.queueMessage("info", msg)
					break
				case 1: // Warning.
					this.queueMessage("warning", msg)
					break
				case 2: // Error.
					this.queueMessage("error", msg)
					break
				default: // We consider other cases as errors, in case of.
					this.queueMessage("error", msg)
					break
			}
		}

		document.getElementById("rewind").addEventListener("click", () => {
			this.rewind()
		})
		document.getElementById("restart").addEventListener("click", () => {
			this.restart()
		})

		this.continue()
	},

	continue() {
		if (this.story.canContinue) {
			this.currentTurnElement = document.createElement("div")
			this.currentTurnElement.classList.add("turn")
			this.storyContainer.appendChild(this.currentTurnElement)
		}

		while (this.story.canContinue) {
			// Advance story.
			let paragraphText = this.story.Continue()

			// Tags.
			if (this.story.currentTags.length) {
				let tagsElement = document.createElement("ul")
				tagsElement.classList.add("tags")
				for (tag of this.story.currentTags) {
					let tagElement = document.createElement("li")
					tagElement.innerText = "# " + tag
					tagsElement.appendChild(tagElement)
				}
				this.currentTurnElement.appendChild(tagsElement)
			}

			// Paragraph.
			let paragraphElement = document.createElement("p")
			paragraphElement.innerText = paragraphText
			this.currentTurnElement.appendChild(paragraphElement)
			// Show pending messages.
			this.showMessages()
		}

		// We save just before the choices so that we don't need to redo the full turn after rewinding.
		this.currentTurnState = this.story.state.ToJson()

		// Choices.
		this.displayChoices()

		this.currentTurnElement.scrollIntoView({
			behavior: "smooth",
			block: "start"
		})

	},

	displayChoices() {
		if (this.story.currentChoices.length === 0) {
			return
		}

		let choicesElement = document.createElement("ul")
		choicesElement.classList.add("choices")

		for (const choice of this.story.currentChoices) {
			let liElement = document.createElement("li")
			let aElement = document.createElement("a")
			aElement.setAttribute("role", "button")
			aElement.innerText = choice.text
			aElement.href = "#_"
			aElement.addEventListener("click", (event) => {
				event.preventDefault()
				choicesElement.remove()
				this.story.ChooseChoiceIndex(choice.index)
				this.history.push(this.currentTurnState)
				if (this.history.length > maxHistoryLength) {
					this.history.shift()
				}
				this.continue()
			})
			liElement.appendChild(aElement)
			choicesElement.appendChild(liElement)
		}

		this.currentTurnElement.appendChild(choicesElement)
	},

	rewind() {
		if (!this.history.length) {
			return
		}

		this.currentTurnState = this.history.pop()
		this.story.state.LoadJson(this.currentTurnState)
		this.currentTurnElement.remove()
		this.currentTurnElement = this.storyContainer.querySelector(".turn:last-child")
		this.displayChoices()
	},

	restart() {
		this.story.ResetState()
		this.history = []
		this.storyContainer.innerText = ""
		this.continue()
	},

	queueMessage(type, contents) {
		this.messages.push({ type, contents })
	},

	showMessages() {
		for ({ type, contents } of this.messages) {
			let paragraphElement = document.createElement("p")
			paragraphElement.classList.add(type)
			paragraphElement.innerText = contents
			this.currentTurnElement.appendChild(paragraphElement)
		}
		this.messages = []
	}
}
