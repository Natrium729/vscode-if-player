/* The official engine uses snake_case so we disable the naming-convention rule. */
/* eslint-disable @typescript-eslint/naming-convention */

window.aaFrontend = {
	engine: window.aaengine,
	status: null,

	mainArea: document.documentElement,
	topStatusBar: document.documentElement,
	inlineStatusBar: document.documentElement,
	currentStatusBar: null, // Is null if we are not in a status bar.
	currentLink: null,

	currentMainDiv: document.documentElement,
	currentStatusDiv: document.documentElement,
	get currentDiv() {
		return this.currentStatusBar ? this.currentStatusDiv : this.currentMainDiv
	},
	set currentDiv(val) {
		if (this.currentStatusBar) {
			this.currentStatusDiv = val
		} else {
			this.currentMainDiv = val
		}
	},

	currentMainSpan: null,
	currentStatusSpan: null,
	get currentSpan() {
		return this.currentStatusBar ? this.currentStatusSpan : this.currentMainSpan
	},
	set currentSpan(val) {
		if (this.currentStatusBar) {
			this.currentStatusSpan = val
		} else {
			this.currentMainSpan = val
		}
	},

	currentMainP: document.documentElement,
	currentStatusP: document.documentElement,
	get currentP() {
		return this.currentStatusBar ? this.currentStatusP : this.currentMainP
	},
	set currentP(val) {
		if (this.currentStatusBar) {
			this.currentStatusP = val
		} else {
			this.currentMainP = val
		}
	},

	// The HTML element in which the text is currently being printed.
	get currentEl() {
		return this.currentSpan ?? this.currentP
	},

	init() {
		this.mainArea = document.getElementById("main")
		this.topStatusBar = document.getElementById("top-status-bar")
		this.inlineStatusBar = document.createElement("div")
		this.inlineStatusBar.id = "inline-status-bar"
		const inlineStatusBarWrapper = document.createElement("div")
		inlineStatusBarWrapper.classList.add("status-bar-wrapper")
		inlineStatusBarWrapper.appendChild(this.inlineStatusBar)
		this.reset()
	},

	submitInput(value) {
		if (this.status !== this.engine.status.get_input) {
			return
		}

		const input = document.getElementById("input")
		if (input) {
			input.remove()
		}

		this.print(value)
		this.par()
		this.status = this.engine.vm_proceed_with_input(value)
		this.currentMainP.scrollIntoView({
			behavior: "smooth",
			block: "start"
		})
		this.setupInput()
	},

	submitKey(code) {
		if (this.status !== this.engine.status.get_key) {
			return
		}

		this.status = this.engine.vm_proceed_with_key(code)
		this.currentMainP.scrollIntoView({
			behavior: "smooth",
			block: "start"
		})
		this.setupInput()
	},

	submitRestore(data) {
		if (this.status !== this.engine.status.restore) {
			return
		}

		this.status = this.engine.vm_restore(data)
		this.currentMainP.scrollIntoView({
			behavior: "smooth",
			block: "start"
		})
		this.setupInput()
	},

	setupInput() {
		switch (this.status) {
			case this.engine.status.get_input:
				const input = document.createElement("input")
				input.type = "text"
				input.id = "input"
				input.addEventListener("keydown", e => {
					switch (e.key) {
						case "Enter": // Send the command to the story.
							e.preventDefault()
							this.submitInput(input.value)
							break
						case "ArrowUp": // Get previous command in the command history.
							// TODO.
							break
						case "ArrowDown": // Get next command in the command history.
							// TODO.
							break
						default:
							// Ignore other keys.
							break
					}
				})
				this.currentMainP.appendChild(input)
				input.focus({ preventScroll: true })
			case this.engine.status.get_key:
				let eventListener = e => {
					e.preventDefault()
					let code = 0
					switch (e.key) {
						case "Backspace":
						case "Delete": // We treat it as backspace in case of. Is that OK?
							code = this.engine.keys.KEY_BACKSPACE
							break
						case "Enter":
							code = this.engine.keys.KEY_RETURN
							break
						case "ArrowUp":
							code = this.engine.keys.KEY_UP
							break
						case "ArrowDown":
							code = this.engine.keys.KEY_DOWN
							break
						case "ArrowLeft":
							code = this.engine.keys.KEY_LEFT
							break
						case "ArrowRight":
							code = this.engine.keys.KEY_RIGHT
							break
						default:
							if (e.key.length === 1) {
								code = e.key.charCodeAt(0)
							}
							break
					}
					document.removeEventListener("keydown", eventListener)
					this.submitKey(code)
				}
				document.addEventListener("keydown", eventListener)
				break
			case this.engine.status.restore:
				/* We don't support restoring. */
				this.submitRestore(null)
				break
			case this.engine.status.quit:
				// Do nothing, I guess?
				break
			default:
				// TODO: Show an error. Should never happen anyway
				break
		}
	},

	/* And now the functions called by the engine. (They roughly follow the section "Output model" of the Å-machine spec.) */

	enter_status(area, style_class) {
		this.currentStatusSpan = null
		this.currentStatusP = document.createElement("p")

		switch (area) {
			case 0: // Top.
				this.currentStatusBar = this.topStatusBar
				break
			case 1: // Inline.
				this.currentStatusBar = this.inlineStatusBar
				break
			default:
				// TODO: Show error? Should never happen anyway.
				break
		}

		this.currentStatusBar.dataset.aaClass = style_class
		this.currentStatusDiv = this.currentStatusBar
		this.currentStatusBar.replaceChildren()
	},

	leave_status() {
		this.flush()
		/* We need to null the current status bar because if it was inline, we need to return to the main area before appending the status bar to it. */
		let statusBar = this.currentStatusBar
		this.currentStatusBar = null
		statusBar.parentNode.hidden = statusBar.innerHTML === ""
		if (statusBar.id === "inline-status-bar") {
			// We take the parent node because we need to take the status bar wrapper.
			this.par()
			this.currentDiv.appendChild(statusBar.parentNode)
		}
	},

	print(str) {
		this.currentEl.append(str)
	},

	space() {
		this.currentEl.append(" ")
	},

	space_n(n) {
		let spacer = document.createElement("span")
		spacer.classList.add("spacer")
		spacer.append(" ".repeat(n))
		this.currentEl.appendChild(spacer)
	},

	line() {
		this.currentEl.appendChild(document.createElement("br"))
	},

	par() {
		if (this.currentSpan) {
			/* The spec allows us to ignore paragraph breaks inside spans. */
			return
		}

		/* We only append the current paragraph if something has been printed in it. (It can happen when we enter multiple divs in a row since entering a div cause a paragraph break, or when leaving a div immediately after opening it.) */
		if (this.currentP.hasChildNodes()) {
			this.currentDiv.appendChild(this.currentP)
			this.currentP = document.createElement("p")
		}
	},

	enter_div(style_class) {
		this.par()
		const div = document.createElement("div")
		div.dataset.aaClass = style_class
		this.currentDiv.appendChild(div)
		this.currentDiv = div
	},

	leave_div(style_class) {
		this.par()
		this.currentDiv = this.currentDiv.parentNode
	},

	enter_span(style_class) {
		const span = document.createElement("span")
		span.dataset.aaClass = style_class
		this.currentEl.appendChild(span)
		this.currentSpan = span
	},

	leave_span() {
		this.currentSpan = this.currentSpan.parentNode
		if (this.currentSpan.tagName === "P") {
			/* If we reached a <p>, then we left all the spans and are back in the current paragraph. */
			this.currentSpan = null
		}
	},

	enter_self_link() {
		let a
		if (this.currentLink) {
			a = document.createElement("span")
		} else {
			a = document.createElement("a")
			a.href = "#_"
			a.dataset.aaLink = ""
			this.currentLink = a
		}
		this.currentEl.appendChild(a)
		this.currentSpan = a
	},

	leave_self_link() {
		const leftLink = this.currentSpan
		this.leave_span()
		if (leftLink === this.currentLink) {
			let cmd = leftLink.textContent
			leftLink.addEventListener("click", e => {
				e.preventDefault()
				this.submitInput(cmd)
			})
			this.currentLink = null
		}
	},

	enter_link(str) {
		let a
		if (this.currentLink) {
			a = document.createElement("span")
		} else {
			a = document.createElement("a")
			a.href = "#_"
			a.dataset.aaLink = ""
			a.addEventListener("click", e => {
				e.preventDefault()
				this.submitInput(str)
			})
			this.currentLink = a
		}
		this.currentEl.appendChild(a)
		this.currentSpan = a
	},

	leave_link() {
		const leftLink = this.currentSpan
		this.leave_span()
		if (leftLink === this.currentLink) {
			this.currentLink = null
		}
	},

	enter_link_res(res) {
		let a
		const isExternal = res.url.match(/^(https?:\/\/)/)
		if (this.currentLink || !isExternal) {
			a = document.createElement("span")
		} else if (isExternal) {
			a = document.createElement("a")
			a.href = res.url
			a.target = "_blank"
			a.dataset.aaLink = ""
			this.currentLink = a
		}
		a.title = res.alt
		this.currentEl.appendChild(a)
		this.currentSpan = a
	},

	leave_link_res() {
		const leftLink = this.currentSpan
		this.leave_span()
		if (leftLink === this.currentLink) {
			this.currentLink = null
		}
	},

	embed_res(res) {
		this.print(`<resource url="${res.url}" alt="${res.alt}">`)
	},

	progressbar(amount, fullAmount) {
		const meter = document.createElement("meter")
		meter.value = amount
		meter.max = fullAmount
		this.currentEl.appendChild(meter)
	},

	set_style(s) {},
	reset_style(s) {},
	unstyle() {},

	clear() {
		/* Empty the current <p> and the current <div>. */
		this.currentP.replaceChildren()
		let currentDiv = this.currentDiv
		currentDiv.replaceChildren()
		/* Empty each ancestor, except for that we keep the current div stack */
		while (currentDiv !== this.mainArea) {
			const parent = currentDiv.parentNode
			currentDiv.remove()
			parent.replaceChildren()
			parent.appendChild(currentDiv)
			currentDiv = parent
		}
	},

	clear_all() {
		this.clear()
		this.topStatusBar.parentNode.hidden = true
		/* No need to clear the status bar, it will be cleared next time we enter it. */
	},

	clear_links() {
		for (a of this.mainArea.querySelectorAll("[data-aa-link]")) {
			a.removeAttribute("href")
		}
	},

	clear_div() {},
	clear_old() {},

	leave_all() {
		// TODO: Make sure it works correctly.
		this.currentMainSpan = null
		this.currentMainDiv = this.mainArea
		this.currentStatusBar = null
		this.currentLink = null
	},

	flush() {
		/* We append the current paragraphs to the document, but future text will still be printed in them until the next `par()`, as usual. */
		if (this.currentMainP.hasChildNodes()) {
			this.currentMainDiv.appendChild(this.currentMainP)
		}
		if (this.currentStatusP.hasChildNodes()) {
			this.currentStatusDiv.appendChild(this.currentStatusP)
		}
	},

	script_on() {
		return false
	},

	script_off() {},

	can_embed_res(res) {
		/* We always return true since the interpreter shows a placeholder for every resource. (It's useful for testing.) */
		return true
	},

	save(data) {
		return false
	},

	restore() {
		/* Nothing to do, since we don't support restore. */
	},

	reset() {
		this.mainArea.replaceChildren()
		this.currentMainDiv = this.mainArea
		this.currentMainP = document.createElement("p")
		this.currentMainSpan = null

		this.currentStatusBar = null
		this.topStatusBar.parentNode.hidden = true
		this.topStatusBar.replaceChildren()
		delete this.topStatusBar.dataset.aaClass
		this.currentStatusDiv = this.topStatusBar
		this.currentStatusP = document.createElement("p")
		this.currentStatusSpan = null
	},

	trace(str) {
		const div = document.createElement("div")
		div.classList.add("trace")
		div.textContent = str
		this.currentEl.appendChild(div)
	},

	have_links() {
		return true
	},
}

window.aaPlayer = {
	frontend: window.aaFrontend,
	engine: window.aaengine,
	status: null,

	start(storyImage) {
		this.frontend.init()
		this.engine.prepare_story(
			storyImage, // The story file data.
			this.frontend, // The I/O layer.
			null, // The random seed; we let the engine initialise it.
			false, // Supports quit.
			true, // Supports status bar.
			true, // Supports inline status.
		)

		// Add the story-specific style to the page.
		let css = ""
		for ([style_class, properties] of this.engine.get_styles().entries()) {
			css += `[data-aa-class="${style_class}"]{`
			for ([prop, val] of Object.entries(properties)) {
				css += `${prop}:${val};`
			}
			css += "}"
		}
		const style_el = document.createElement("style")
		style_el.textContent = css
		document.head.appendChild(style_el)
		this.frontend.status = this.engine.async_restart()
		this.frontend.setupInput()
	},
}
