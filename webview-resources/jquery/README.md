# Quixe

This folder contains jQuery 1.12.4, as used in Quixe by Andrew Plotkin (based on the Inform 7 template downloadable on the [Quixe homepage](https://eblong.com/zarf/glulx/quixe/)).

## License

The jQuery JavaScript framework, version 1.12.4. Copyright jQuery Foundation and other contributors. Released under the [MIT license](http://jquery.org/license). For details, see [the jQuery web site](http://jquery.com/).
