/* eslint-disable @typescript-eslint/naming-convention */

(function() {
	const vscode = acquireVsCodeApi()

	let storyStarted = false
	let storyName = "UNKNOWN"

	function decodeBase64(base64Data) {
		const data = window.atob(base64Data)
		const bytes = new Uint8Array(data.length)
		let i
		for (i = 0; i < data.length; i++) {
			bytes[i] = data.charCodeAt(i)
		}
		return bytes.buffer
	}

	function startGlkOteStory(terp, storyImage) {
		if (!storyImage) {
			window.GlkOte.error(`The file "${storyName}" couldn't be found.`)
			return
		}

		const game_options = {
			// TODO: see if it's necessary to extract images from blorb.
			// image_info_map: 'StaticImageInfo',
			use_query_story: false,
			log_execution_time: true,
			set_page_title: false,
			exit_warning: "The game session has ended. Click on the reload button to restart.",
			inspacing: 0,
			outspacing: 10
		}

		switch (terp) {
			case "quixe":
				GiLoad.load_run(game_options, storyImage, "base64")
				break
			case "zvm":
				game_options.vm = new window.ZVM()
				game_options.Glk = window.Glk
				game_options.vm.prepare(
					decodeBase64(storyImage),
					game_options
				)
				game_options.Glk.init(game_options)
				break
			default:
				window.GlkOte.error(`BUG: the interpreter ${terp} does not exist.`)
				break
		}
	}

	function startInkStory(storyData) {
		window.inkPlayer.start(storyData)
	}

	function startAaStory(storyData) {
		/* For Å-machine stories, `storyData` is supposed to be a `Uint8Array` if the `engines.vscode` field of package.json is 1.57. It doesn't seem to be the case and the data is in the `data` property instead. We try both just to be on the safe side. */
		window.aaPlayer.start(storyData.data ?? storyData)
	}

	function startStory(storyData, id) {
		if (storyStarted) {
			return
		}
		storyStarted = true

		switch (id) {
			case "quixe":
			case "zvm":
				startGlkOteStory(id, storyData)
				break
			case "ink":
				startInkStory(storyData)
				break
			case "aa-machine":
				startAaStory(storyData)
				break
			default:
				break
		}
	}

	function reloadWebview() {
		vscode.postMessage({
			command: "reload-webview"
		})
	}

	function updateAppearance(style) {
		for (const key of Object.keys(style)) {
			if (style[key] === null || style[key] === undefined) {
				document.documentElement.style.removeProperty(key)
			} else {
				document.documentElement.style.setProperty(key, style[key])
			}
		}
	}

	document.addEventListener("DOMContentLoaded", () => {
		/* TODO: Because of the hack to hide #errorpane (see glkote.css), GlkOte.error doesn't show the error anymore. We patch it to use jQuery (because for some reason that works. */
		if (window.Glkote !== undefined) {
			const oldGlkoteError = window.GlkOte.error
			window.GlkOte.error = function(msg) {
				oldGlkoteError(msg)
				if (window.$) {
					window.$("#errorpane").show()
				}
			}
		}

		window.addEventListener("message", event => {
			switch (event.data.command) {
				case "start-story":
					storyName = event.data.payload.storyName
					updateAppearance(event.data.payload.initialStyle)
					startStory(
						event.data.payload.storyContents,
						event.data.payload.id
					)
					break
				case "update-appearance":
					updateAppearance(event.data.payload)
					break
			}
		})

		document.getElementById("reload-webview").addEventListener("click", () => {
			reloadWebview()
		})

		vscode.postMessage({ command: "ready" })
	})
}())
