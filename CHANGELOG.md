# Changelog

## 0.3.0

### Added

- Support for Å-machine story files (`.aastory` file extension).

## 0.2.0

### Added

- Compiled ink files (`.ink.json` extension) are supported.

## 0.1.0

- Initial release.
