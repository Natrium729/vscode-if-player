/* This file contains the base player custom editor implementing the common functionality of all the players, which inherit from it. */

import path = require("path")

import * as vscode from "vscode"

import { getStyleUpdate, WebviewPanelCollection } from "./utils"


/**
 * The `CustomDocument` used by every player custom editors provided by this extension.
 *
 * Since all documents behave the same, we don't need a different one for each custom editor.
 */
export class PlayerDocument implements vscode.CustomDocument {
	readonly uri: vscode.Uri
	readonly disposables: vscode.Disposable[] = []

	private _contents: Uint8Array
	get contents(): Uint8Array {
		return this._contents
	}

	constructor(uri: vscode.Uri) {
		this.uri = uri
		this._contents = new Uint8Array(0)
	}

	static async fromUri(uri: vscode.Uri): Promise<PlayerDocument> {
		const document = new this(uri)
		await document.refreshContents()
		return document
	}

	async refreshContents() {
		try {
			this._contents = await vscode.workspace.fs.readFile(this.uri)
		} catch {
			// Fallback to an empty document if there were an error.
			this._contents = new Uint8Array(0)
		}
	}

	dispose() {
		for (const disposable of this.disposables) {
			disposable.dispose()
		}
	}
}


/**
 * The base Custom editor provider that other should inherit.
 *
 * Subclasses should override the `documentEncoding` property, and the `getHtmlForWebview` method.
 */
export class BasePlayerEditorProvider implements vscode.CustomReadonlyEditorProvider {
	protected readonly id: string
	protected readonly documentEncoding: BufferEncoding | undefined = undefined
	// TODO: maybe pick what we need in the context instead of grabbing the whole of it.
	protected readonly context: vscode.ExtensionContext
	private readonly webviewPanels: WebviewPanelCollection = new WebviewPanelCollection()

	static register(viewType: string, id: string, context: vscode.ExtensionContext) {
		return vscode.window.registerCustomEditorProvider(
			viewType,
			new this(id, context),
			{
				webviewOptions: {
					// So that the game state is kept when switching tabs.
					retainContextWhenHidden: true,
				},
				supportsMultipleEditorsPerDocument: false
			}
		)
	}

	constructor(id: string, context: vscode.ExtensionContext) {
		this.id = id
		this.context = context

		/* Update the appearance of the webview when the relevant configuration changes */
		vscode.workspace.onDidChangeConfiguration(event => {
			if (event.affectsConfiguration("ifPlayer.appearance") && this.webviewPanels.entries.length) {
				const styleUpdate = getStyleUpdate()
				for (const { panel } of this.webviewPanels.entries) {
					panel.webview.postMessage({
						command: "update-appearance",
						payload: styleUpdate
					})
				}
			}
		}, this, context.subscriptions)
	}

	async openCustomDocument(
		uri: vscode.Uri,
		openContext: vscode.CustomDocumentOpenContext,
		token: vscode.CancellationToken
	): Promise<PlayerDocument> {
		const document = await PlayerDocument.fromUri(uri)

		/* If the document is in a workspace folder, watch it and reload the webview when it changes.
		TODO: Fore some reason, the onDidChange fires twice in a row sometimes, when it should not. */
		if (vscode.workspace.asRelativePath(uri) !== uri.fsPath) {
			const fileWatcher = vscode.workspace.createFileSystemWatcher(document.uri.fsPath, true, false, true)
			document.disposables.push(fileWatcher)
			fileWatcher.onDidChange(async () => {
				await document.refreshContents()
				for (const panel of this.webviewPanels.getWebviewPanelsForUri(uri)) {
					this.reloadWebview(panel.webview)
				}
			}, this, document.disposables)
		}

		return document
	}

	resolveCustomEditor(document: PlayerDocument, webviewPanel: vscode.WebviewPanel, token: vscode.CancellationToken) {
		this.webviewPanels.add(document.uri, webviewPanel)
		const webview = webviewPanel.webview

		webview.options = {
			enableScripts: true,
			// TODO: Limit to the relevant subfolders only (depending on the editor)?
			localResourceRoots: [vscode.Uri.file(path.join(this.context.extensionPath, "webview-resources/"))]
		}
		webview.html = this.getHtmlForWebview(webview)

		webview.onDidReceiveMessage(async message => {
			switch (message.command) {
				case "ready":
					let contents = this.documentEncoding
						? Buffer.from(document.contents)
							.toString(this.documentEncoding).replace(/^\uFEFF/, "")
							/* When reading as a string, we strip the BOM because inklecate compiles ink files with one, but Node.js has problems with that. That might cause unexpected results depending of the encoding, but it's OK for the one we use (utf8 and base64). */
						: document.contents
					webview.postMessage({
						command: "start-story",
						payload: {
							id: this.id,
							storyName: path.basename(document.uri.fsPath),
							storyContents: contents,
							initialStyle: getStyleUpdate()
						}
					})
					break
				case "reload-webview":
					document.refreshContents()
					this.reloadWebview(webview)
					break
			}
		}, this, this.context.subscriptions)
	}

	reloadWebview(webview: vscode.Webview) {
		/* Re-set the HTML to reload the entire webview. */
		webview.html = this.getHtmlForWebview(webview)
	}

	getHtmlForWebview(webview: vscode.Webview): string {
		/* We return a default HTML saying that we forgot to override this method in the subclass. */
		return `<!DOCTYPE html>
			<html>
			<head>
				<meta charset='utf-8'>

				<meta
					http-equiv="Content-Security-Policy"
					content="default-src 'none'"
				/>

				<title>ERROR: Non-overridden webview contents!</title>
				<meta name='viewport' content='width=device-width, initial-scale=1'>
			</head>
			<body>
				The IF Player extension's author forgot to override this HTML in subclass!
			</body>
			</html>`
	}
}
