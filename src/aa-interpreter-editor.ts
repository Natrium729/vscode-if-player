import path = require("path")

import * as vscode from "vscode"

import { BasePlayerEditorProvider } from "./base-player-editor"
import { getNonce } from "./utils"


/* The custom editor specified in this file is the one for opening games that use the official Å-machine JavaScript interpreter engine. */

export class AaInterpreterEditorProvider extends BasePlayerEditorProvider {
	getHtmlForWebview(webview: vscode.Webview): string {
		const webviewResourcesUri = path.join(this.context.extensionPath, "webview-resources")

		const aaCssUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "aa-machine/aa-frontend.css")
		))
		const aaEngineJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "aa-machine/aa-engine.js")
		))
		const aaFrontendJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "aa-machine/aa-frontend.js")
		))
		const vscodeInterfaceJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "vscode-interface.js")
		))

		const nonce = getNonce()

		/* We allow inline styles in the CSP because the interpreter will add the styles embedded in the story file into the page's head. */

		return `<!DOCTYPE html>
			<html>
			<head>
				<meta charset='utf-8'>

				<meta
					http-equiv="Content-Security-Policy"
					content="default-src 'none'; img-src ${webview.cspSource}; script-src 'nonce-${nonce}'; style-src ${webview.cspSource} 'unsafe-inline';"
				/>

				<title>${this.id}</title>
				<meta name='viewport' content='width=device-width, initial-scale=1'>

				<link rel="stylesheet" href="${aaCssUri}" type="text/css">
				<script nonce="${nonce}" src="${aaEngineJsUri}"></script>
				<script nonce="${nonce}" src="${aaFrontendJsUri}"></script>
				<script nonce="${nonce}" src="${vscodeInterfaceJsUri}"></script>
			</head>
			<body>
				<div id="gameport">
					<div id="top-status-bar-wrapper" class="status-bar-wrapper" hidden>
						<div id="top-status-bar"></div>
					</div>
					<div id="main">Loading...</div>
					<button id="reload-webview" type="button">Reload</button>
				</div>
			</body>
			</html>`
	}
}
