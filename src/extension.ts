import * as vscode from "vscode"

import { GlkoteInterpreterEditorProvider } from "./glkote-interpreter-editor"
import { AaInterpreterEditorProvider } from "./aa-interpreter-editor"
import { InkPlayerEditorProvider } from "./ink-player-editor"


export function activate(context: vscode.ExtensionContext) {
	context.subscriptions.push(GlkoteInterpreterEditorProvider.register(
		"ifPlayer.quixe",
		"quixe",
		context
	))
	context.subscriptions.push(GlkoteInterpreterEditorProvider.register(
		"ifPlayer.zvm",
		"zvm",
		context
	))
	context.subscriptions.push(AaInterpreterEditorProvider.register(
		"ifPlayer.aaMachine",
		"aa-machine",
		context
	))
	context.subscriptions.push(InkPlayerEditorProvider.register(
		"ifPlayer.ink",
		"ink",
		context
	))
}
