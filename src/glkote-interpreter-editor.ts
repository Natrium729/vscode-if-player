import path = require("path")

import * as vscode from "vscode"

import { BasePlayerEditorProvider } from "./base-player-editor"
import { getNonce } from "./utils"


/* The custom editor specified in this file is one for opening games that use an interpreter with GlkOte that has the same API than Quixe.

It is used for Quixe and ZVM. */

export class GlkoteInterpreterEditorProvider extends BasePlayerEditorProvider {
	protected readonly documentEncoding = "base64"

	getHtmlForWebview(webview: vscode.Webview): string {
		const webviewResourcesUri = path.join(this.context.extensionPath, "webview-resources")

		const glkoteCssUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "glkote/glkote.css")
		))
		const dialogCssUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "glkote/dialog.css")
		))
		const jqueryJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "jquery/jquery.min.js")
		))
		const glkoteJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "glkote/glkote.min.js")
		))
		const terpJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, `${this.id}/${this.id}.min.js`)
		))
		const vscodeInterfaceJsUri = webview.asWebviewUri(vscode.Uri.file(
			path.join(webviewResourcesUri, "vscode-interface.js")
		))

		const nonce = getNonce()

		/* Below we authorise "unsafe-eval" in the CSP because the interpreter needs it. */

		return `<!DOCTYPE html>
			<html>
			<head>
				<meta charset='utf-8'>

				<meta
					http-equiv="Content-Security-Policy"
					content="default-src 'none'; img-src ${webview.cspSource}; script-src 'nonce-${nonce}' 'unsafe-eval'; style-src ${webview.cspSource};"
				/>

				<title>${this.id}</title>
				<meta name='viewport' content='width=device-width, initial-scale=1'>

				<link rel="stylesheet" href="${glkoteCssUri}" type="text/css">
				<link rel="stylesheet" href="${dialogCssUri}" type="text/css">
				<script nonce="${nonce}" src="${jqueryJsUri}"></script>
				<script nonce="${nonce}" src="${glkoteJsUri}"></script>
				<script nonce="${nonce}" src="${terpJsUri}"></script>
				<script nonce="${nonce}" src="${vscodeInterfaceJsUri}"></script>
			</head>
			<body>
				<div id="gameport">
					<div id="windowport"></div>
					<div id="loadingpane">
						<em>&nbsp;&nbsp;&nbsp;Loading...</em>
					</div>
					<div id="errorpane" style="display:none;">
						<div id="errorcontent">...</div>
					</div>
					<button id="reload-webview" type="button">Reload</button>
				</div>
			</body>
			</html>`
	}
}
