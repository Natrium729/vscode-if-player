import * as vscode from "vscode"


export function getNonce() {
	let text = ""
	const possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
	for (let i = 0; i < 32; i++) {
		text += possible.charAt(Math.floor(Math.random() * possible.length))
	}
	return text
}


export class WebviewPanelCollection {
	private readonly _entries = new Set<{
		resource: string
		panel: vscode.WebviewPanel
	}>()

	// Return a copy so that no-one outside the class can modify the real entries array.
	get entries() {
		return Array.from(this._entries)
	}

	add(uri: vscode.Uri, panel: vscode.WebviewPanel) {
		const entry = {
			resource: uri.toString(),
			panel
		}
		// TODO: Prevent adding the panel if it's already in the set?
		this._entries.add(entry)
		panel.onDidDispose(() => {
			this._entries.delete(entry)
		})
	}

	getWebviewPanelsForUri(uri: vscode.Uri): vscode.WebviewPanel[] {
		const resource = uri.toString()
		const matching: vscode.WebviewPanel[] = []
		for (const entry of this._entries) {
			if (entry.resource === resource) {
				matching.push(entry.panel)
			}
		}
		return matching
	}
}


export function getStyleUpdate() {
	const config = vscode.workspace.getConfiguration("ifPlayer.appearance")
	const styleUpdate: any = {}

	const fontSize = config.get<number>("fontSize", 0)
	styleUpdate["--font-size"] = fontSize ? `${fontSize}px` : null

	styleUpdate["--font-family"] = config.get<string>("fontFamily", "") || null

	styleUpdate["--font-family-monospace"] = config.get<string>("fontFamilyMonospace", "") || null

	styleUpdate["--line-height"] = config.get<number>("lineHeight", 0) || null

	return styleUpdate
}
