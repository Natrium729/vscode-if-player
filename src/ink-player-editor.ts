import path = require("path")

import * as vscode from "vscode"

import { BasePlayerEditorProvider } from "./base-player-editor"
import { getNonce } from "./utils"


// The custom editor specified in this file is one for opening JSON files created by ink.


export class InkPlayerEditorProvider extends BasePlayerEditorProvider {
	protected documentEncoding: BufferEncoding = "utf8"

	getHtmlForWebview(webview: vscode.Webview): string {
		const webviewResourcesUri = vscode.Uri.joinPath(this.context.extensionUri, "webview-resources")

		const inkCssUri = webview.asWebviewUri(vscode.Uri.joinPath(
			webviewResourcesUri,
			"ink",
			"ink-player.css"
		))
		const inkJsUri = webview.asWebviewUri(vscode.Uri.joinPath(
			webviewResourcesUri,
			"ink",
			"inkjs",
			"ink.js"
		))
		const inkPlayerJsUri = webview.asWebviewUri(vscode.Uri.joinPath(
			webviewResourcesUri,
			"ink",
			"ink-player.js"
		))
		const vscodeInterfaceJsUri = webview.asWebviewUri(vscode.Uri.joinPath(
			webviewResourcesUri,
			"vscode-interface.js"
		))

		const nonce = getNonce()

		return `<!DOCTYPE html>
			<html>
			<head>
				<meta charset='utf-8'>

				<meta
					http-equiv="Content-Security-Policy"
					content="default-src 'none'; img-src ${webview.cspSource}; script-src 'nonce-${nonce}'; style-src ${webview.cspSource};"
				/>

				<title>${this.id}</title>
				<meta name='viewport' content='width=device-width, initial-scale=1'>

				<link rel="stylesheet" href="${inkCssUri}" type="text/css">
				<script nonce="${nonce}" src="${inkJsUri}"></script>
				<script nonce="${nonce}" src="${inkPlayerJsUri}"></script>
				<script nonce="${nonce}" src="${vscodeInterfaceJsUri}"></script>
			</head>
			<body>
				<nav id="navbar">
					<button id="rewind" type="button">Rewind</button>
					<button id="restart" type="button">Restart</button>
					<button id="reload-webview" type="button">Reload</button>
				</nav>
				<main id="story-container" aria-live="polite" aria-atomic="false" aria-relevant="additions removals"></main>
			</body>
			</html>`
	}
}
